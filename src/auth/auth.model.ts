import { prop } from "@typegoose/typegoose";
import { Base, TimeStamps } from "@typegoose/typegoose/lib/defaultClasses";

export interface AuthModel extends Base {}

export class AuthModel extends TimeStamps {
    /* _id: string; */

    @prop()
    email: string;

    @prop()
    passwordHash: string;
}
